# NewTrient

NewTrient is a web app that tracks you calorie intake. You can schedule meals, create templates for meals and increment those. The app calculates your total calory consumption automatically.


Colors:

Green:  #55FA5A
Yellow: #D9C34A
Orange: #F08E5D
Purple: #D44AD9
Blue:   #567DFC
